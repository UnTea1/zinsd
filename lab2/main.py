import csv, re

if __name__ == '__main__':
    dns_log = open('dns.log')
    hosts = open('allHosts')
    dns = csv.reader(dns_log, delimiter="\x09")
    count, n, bad_hosts = 0, 0, []
    regular = re.compile('[^a-zA-Z.]')

    for line in hosts.readlines():
        line = re.sub(r'\[[^][]*\]', '', line)

        if line != '\n':
            bad_hosts.append(regular.sub('', line))

    for row in dns:
        try:
            n += 1

            if row[9] in bad_hosts:
                print(row[9])
                count += 1

        except IndexError:
            pass

    print(f"Процент нежелательного трафика равен {count * 100.0 / n:.05} %")

    dns_log.close()
    hosts.close()
